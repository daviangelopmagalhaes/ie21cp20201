# **Cooler Controlado pela Tempertaura**

Sistema que liga e desliga um cooler a partir de certa temperatura informada por um sensor

## Equipe

O projeto foi desenvolvido pelos alunos de Engenharia de Computação:

|Nome| GitLab User|
|---|---|
|Davi Angelo Polese Magalhães|@daviangelopmagalhaes|
|Erick Walace de Melo|@erickmelo1|



## Documentação

A documentação do projeto pode ser acessada pelo seguinte link:

>  https://daviangelopmagalhaes.gitlab.io/ie21cp20201/


---

### Links Úteis

 * [Projeto no Tinkercad](https://www.tinkercad.com/things/0AcYUpwIqnO-cooler-controlado-pela-temperatura)


