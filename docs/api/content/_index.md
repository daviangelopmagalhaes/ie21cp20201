# Documentação de Projeto de Cooler Controlado pela Temperatura

## 1. Introdução

Projeto desenvolvido na disciplina de Introdução a Engenharia no curso de Engenharia de Computação da UTFPR

#### Implementação do projeto:

![](Projeto.png)


## 2. Objetivos

 * Desenvolver um sistema que consiga ligar e desligar um cooler a partir de certa temperatura
 * Possibitar ao usuário ligar ou desligar o sitema por um botão
 * Informar a temperatura e o estado do cooler em um display

## 3. Materiais utilizados

### Lista de Materiais
 * Arduino Uno R3
 * Protoboard
 * Display LCD 16x2
 * Potenciômetro 10kΩ
 * 3 resistores (1 de 150Ω, 1 de 220Ω e um de 10KΩ)
 * LED vermelho
 * Push Button
 * Sensor de Temperatura
 * Cooler

## 4. Esquema Elétrico


![](Projeto.png)

## 5. Código

```Cpp
#include <LiquidCrystal.h>

LiquidCrystal lcd(12,11,5,4,3,2); //Pinos do display

#define temp A0
#define cooler 13
#define botao 7
#define led 8
#define controle 35  //Temperatura que o cooler ira ligar

int estado=0;
float temperatura=0;

byte grau[8]= { //Declaracao simbolo de grau
	0b01110,
	0b01010,
	0b01110,
	0b00000,
	0b00000,
	0b00000,
	0b00000,
	0b00000
};

void setup()
{
  lcd.begin(16,2);
  pinMode(botao, INPUT);
  pinMode(cooler, OUTPUT);
  pinMode(led, OUTPUT);
  
  lcd.createChar(1, grau);
  lcd.noDisplay();
}

void loop()
{ 
  if(digitalRead(botao)==HIGH) //Botao iniciar/desligar
  {
    lcd.clear();
    if(estado==0) //Iniciar
    {
      digitalWrite(led, HIGH);
      lcd.display();
      estado=1;
    }
    else //Desligar
    {
      digitalWrite(led, LOW);
      digitalWrite(cooler, LOW);
      lcd.noDisplay();
      estado=0;
    }
    while(digitalRead(botao)==HIGH); //Aguarda o usuario soltar o botao
  }
  
  if(estado==1) //Executa somente quando ligado
  {
    temperatura=map(analogRead(temp), 20, 358, -40 ,125); //Calcula a temperatura a partir do sinal do sensor

    lcd.setCursor(3,0);
    lcd.print("Temp: ");
    if(temperatura<0) //Ajustar temperaturas negativas ao display
    {
      lcd.setCursor(8,0);
    }
    else
    {
      lcd.setCursor(9,0);
    }
    lcd.print(temperatura, 0);
    lcd.write(1); //Simbolo de grau
    lcd.print("C  "); 
    
    if(temperatura>controle) //Ligar e desligar cooler
    {
      digitalWrite(cooler, HIGH);
      lcd.setCursor(3,1);
      lcd.print("Cooler ON");
    }
    else
    {
      digitalWrite(cooler, LOW);
      lcd.setCursor(3,1);
      lcd.print("         ");
    }
  }
  delay(100);
}
```

## 6. Resultados

#### O vídeo de demonstração:
{{< youtube in-Nrls4D5A >}}


## 7. Desafios encontrados

Entender o funcionamento do Display 

Organizar os componentes de uma maneira facil e agradavel de compreender

Compreender certas partes com pouco conhecimento de elétrica